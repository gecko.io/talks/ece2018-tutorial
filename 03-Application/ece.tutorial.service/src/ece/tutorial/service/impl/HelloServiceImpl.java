/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.tutorial.service.impl;

import org.osgi.service.component.annotations.*;

import ece.tutorial.service.api.HelloService;

@Component(scope=ServiceScope.PROTOTYPE)
public class HelloServiceImpl implements HelloService{

	/* 
	 * (non-Javadoc)
	 * @see ece.tutorial.service.api.HelloService#sayHello(java.lang.String)
	 */
	@Override
	public String sayHello(String name) {
		return "Hello " + name;
	}

}
