/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.tutorial.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import ece.tutorial.service.api.HelloService;

@RunWith(MockitoJUnitRunner.class)
public class HelloWorldIntegrationTest {

	private final BundleContext context = FrameworkUtil.getBundle(HelloWorldIntegrationTest.class).getBundleContext();

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	@Test
	public void testHelloWorld() {
		HelloService service = getService();
		assertNotNull(service);
		assertEquals("Hello test", service.sayHello("test"));
	}
	
	private HelloService getService() {
		ServiceTracker<HelloService, HelloService> hst = new ServiceTracker<HelloService, HelloService>(context, HelloService.class, null);
		hst.open();
		return hst.getService();
	}

}