/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.tutorial.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

/**
 * 
 * @author mark
 * @since 20.10.2018
 */
@JaxrsResource
@JaxrsName("welcome")
@Path("/")
@Component(service=WelcomeResource.class)
public class WelcomeResource {

	@GET
	@Path("/welcome")
	@Produces(MediaType.APPLICATION_JSON)
	public Response test(@QueryParam("first") String firstName, @QueryParam("last") String lastName) {
		return Response.ok(String.format("{\"firstName\":\"%s\", \"lastname\":\"%s\"}", firstName, lastName)).build();
//		Person p = new Person();
//		p.setFirstName(firstName);
//		p.setLastName(lastName);
//		return Response.ok(p).build();
	}

}
