/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.tutorial.rest.reader;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import ece.tutorial.service.api.Person;

/**
 * 
 * @author mark
 * @since 20.10.2018
 */
//@JaxrsExtension
//@JaxrsName("JacksonPersonWriter")
//@Produces(MediaType.APPLICATION_JSON)
//@Component(service=MessageBodyWriter.class)
//@Component(service=MessageBodyWriter.class, property= {"service.rank=10"})
public class JacksonJsonMBW implements MessageBodyWriter<Person> {

	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyWriter#isWriteable(java.lang.Class, java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2, MediaType arg3) {
		return Person.class.isAssignableFrom(arg0);
	}

	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyWriter#writeTo(java.lang.Object, java.lang.Class, java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType, javax.ws.rs.core.MultivaluedMap, java.io.OutputStream)
	 */
	@Override
	public void writeTo(Person p, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4,
			MultivaluedMap<String, Object> arg5, OutputStream arg6) throws IOException, WebApplicationException {
//		ObjectMapper mapper = new ObjectMapper(new JsonFactory());
//		p.setFirstName(p.getFirstName() + " Jackson");
//		mapper.writeValue(arg6, p);
	}

}
