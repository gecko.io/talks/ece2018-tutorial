/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package ece.tutorial.rest.reader;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import ece.tutorial.service.api.Person;

/**
 * 
 * @author mark
 * @since 20.10.2018
 */
//@JaxrsExtension
//@JaxrsName("SimplePersonJsonWriter")
//@Produces(MediaType.APPLICATION_JSON)
//@Component(service=MessageBodyWriter.class)
//@Component(service=MessageBodyWriter.class, property= {"service.rank=5"})
public class MySimplJsonMBW implements MessageBodyWriter<Person> {

	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyWriter#isWriteable(java.lang.Class, java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2, MediaType arg3) {
		return Person.class.isAssignableFrom(arg0);
	}

	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyWriter#writeTo(java.lang.Object, java.lang.Class, java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType, javax.ws.rs.core.MultivaluedMap, java.io.OutputStream)
	 */
	@Override
	public void writeTo(Person p, Class<?> arg1, Type arg2, Annotation[] arg3, MediaType arg4,
			MultivaluedMap<String, Object> arg5, OutputStream out) throws IOException, WebApplicationException {
		
		String value = String.format("{\"firstName\":\"%s Simple\", \"lastname\":\"%s\"}", p.getFirstName(), p.getLastName());
		out.write(value.getBytes());
		out.flush();
	}

}
