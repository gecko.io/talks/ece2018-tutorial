/**
 */
package {{basePackageName}}.{{EcorePackageName}}.impl;

import {{basePackageName}}.{{EcorePackageName}}.Example;
import {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Example</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExampleImpl extends MinimalEObjectImpl.Container implements Example {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExampleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return {{GenModelPrefix}}Package.Literals.EXAMPLE;
	}

} //ExampleImpl
