/**
 */
package {{basePackageName}}.{{EcorePackageName}};

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Factory
 * @model kind="package"
 * @generated
 */
public interface {{GenModelPrefix}}Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "{{EcorePackageName}}";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "{{EcoreURI}}";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "{{EcoreNSPrefix}}";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	{{GenModelPrefix}}Package eINSTANCE = {{basePackageName}}.{{EcorePackageName}}.impl.{{GenModelPrefix}}PackageImpl.init();

	/**
	 * The meta object id for the '{@link {{basePackageName}}.{{EcorePackageName}}.impl.ExampleImpl <em>Example</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see {{basePackageName}}.{{EcorePackageName}}.impl.ExampleImpl
	 * @see {{basePackageName}}.{{EcorePackageName}}.impl.{{GenModelPrefix}}PackageImpl#getExample()
	 * @generated
	 */
	int EXAMPLE = 0;

	/**
	 * The number of structural features of the '<em>Example</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Example</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link {{basePackageName}}.{{EcorePackageName}}.Example <em>Example</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Example</em>'.
	 * @see {{basePackageName}}.{{EcorePackageName}}.Example
	 * @generated
	 */
	EClass getExample();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	{{GenModelPrefix}}Factory get{{GenModelPrefix}}Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link {{basePackageName}}.{{EcorePackageName}}.impl.ExampleImpl <em>Example</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see {{basePackageName}}.{{EcorePackageName}}.impl.ExampleImpl
		 * @see {{basePackageName}}.{{EcorePackageName}}.impl.{{GenModelPrefix}}PackageImpl#getExample()
		 * @generated
		 */
		EClass EXAMPLE = eINSTANCE.getExample();

	}

} //{{GenModelPrefix}}Package
