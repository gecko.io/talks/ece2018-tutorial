/**
 */
package {{basePackageName}}.{{EcorePackageName}}.impl;

import {{basePackageName}}.{{EcorePackageName}}.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class {{GenModelPrefix}}FactoryImpl extends EFactoryImpl implements {{GenModelPrefix}}Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static {{GenModelPrefix}}Factory init() {
		try {
			{{GenModelPrefix}}Factory the{{GenModelPrefix}}Factory = ({{GenModelPrefix}}Factory)EPackage.Registry.INSTANCE.getEFactory({{GenModelPrefix}}Package.eNS_URI);
			if (the{{GenModelPrefix}}Factory != null) {
				return the{{GenModelPrefix}}Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new {{GenModelPrefix}}FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public {{GenModelPrefix}}FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case {{GenModelPrefix}}Package.EXAMPLE: return createExample();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Example createExample() {
		ExampleImpl example = new ExampleImpl();
		return example;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public {{GenModelPrefix}}Package get{{GenModelPrefix}}Package() {
		return ({{GenModelPrefix}}Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static {{GenModelPrefix}}Package getPackage() {
		return {{GenModelPrefix}}Package.eINSTANCE;
	}

} //{{GenModelPrefix}}FactoryImpl
