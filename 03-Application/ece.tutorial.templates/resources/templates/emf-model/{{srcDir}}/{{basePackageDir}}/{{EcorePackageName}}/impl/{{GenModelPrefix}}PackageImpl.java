/**
 */
package {{basePackageName}}.{{EcorePackageName}}.impl;

import {{basePackageName}}.{{EcorePackageName}}.Example;
import {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Factory;
import {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class {{GenModelPrefix}}PackageImpl extends EPackageImpl implements {{GenModelPrefix}}Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exampleEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private {{GenModelPrefix}}PackageImpl() {
		super(eNS_URI, {{GenModelPrefix}}Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link {{GenModelPrefix}}Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static {{GenModelPrefix}}Package init() {
		if (isInited) return ({{GenModelPrefix}}Package)EPackage.Registry.INSTANCE.getEPackage({{GenModelPrefix}}Package.eNS_URI);

		// Obtain or create and register package
		{{GenModelPrefix}}PackageImpl the{{GenModelPrefix}}Package = ({{GenModelPrefix}}PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof {{GenModelPrefix}}PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new {{GenModelPrefix}}PackageImpl());

		isInited = true;

		// Create package meta-data objects
		the{{GenModelPrefix}}Package.createPackageContents();

		// Initialize created meta-data
		the{{GenModelPrefix}}Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		the{{GenModelPrefix}}Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put({{GenModelPrefix}}Package.eNS_URI, the{{GenModelPrefix}}Package);
		return the{{GenModelPrefix}}Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExample() {
		return exampleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public {{GenModelPrefix}}Factory get{{GenModelPrefix}}Factory() {
		return ({{GenModelPrefix}}Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		exampleEClass = createEClass(EXAMPLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(exampleEClass, Example.class, "Example", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //{{GenModelPrefix}}PackageImpl
