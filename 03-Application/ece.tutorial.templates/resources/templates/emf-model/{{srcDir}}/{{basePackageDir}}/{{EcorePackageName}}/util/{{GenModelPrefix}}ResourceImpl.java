/**
 */
package {{basePackageName}}.{{EcorePackageName}}.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see {{basePackageName}}.{{EcorePackageName}}.util.{{GenModelPrefix}}ResourceFactoryImpl
 * @generated
 */
public class {{GenModelPrefix}}ResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public {{GenModelPrefix}}ResourceImpl(URI uri) {
		super(uri);
	}

} //{{GenModelPrefix}}ResourceImpl
