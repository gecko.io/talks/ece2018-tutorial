/**
 */
package {{basePackageName}}.{{EcorePackageName}};

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package
 * @generated
 */
public interface {{GenModelPrefix}}Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	{{GenModelPrefix}}Factory eINSTANCE = {{basePackageName}}.{{EcorePackageName}}.impl.{{GenModelPrefix}}FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Example</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Example</em>'.
	 * @generated
	 */
	Example createExample();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	{{GenModelPrefix}}Package get{{GenModelPrefix}}Package();

} //{{GenModelPrefix}}Factory
