/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package {{basePackageName}}.{{EcorePackageName}}.configuration;

import {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package;
import org.gecko.emf.osgi.EPackageConfigurator;
import org.gecko.emf.osgi.annotation.EMFModel;
import org.gecko.emf.osgi.annotation.provide.ProvideEMFModel;
import org.gecko.emf.osgi.annotation.require.RequireEMF;
import org.osgi.service.component.annotations.Component;

/**
 * <!-- begin-user-doc -->
 * The <b>EPackageConfiguration</b> and <b>ResourceFactoryConfigurator</b> for the model.
 * The package will be registered into a OSGi base model registry.
 * <!-- end-user-doc -->
 * @see EPackageConfigurator
 * @see ResourceFactoryConfigurator
 * @generated
 */
@Component(name="{{GenModelPrefix}}Configurator", immediate=true, service=EPackageConfigurator.class)
@EMFModel(emf_model_name = {{GenModelPrefix}}Package.eNAME, emf_model_nsURI = {{GenModelPrefix}}Package.eNS_URI, emf_model_version = "1.0")
@RequireEMF
@ProvideEMFModel(name = {{GenModelPrefix}}Package.eNAME, nsURI = {{GenModelPrefix}}Package.eNS_URI, version = "1.0")
public class {{GenModelPrefix}}ConfigurationComponent implements EPackageConfigurator {

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.EPackageConfigurator#configureEPackage(org.eclipse.emf.ecore.EPackage.Registry)
	 */
	@Override
	public void configureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {
		{{GenModelPrefix}}Package.eINSTANCE.getClass();
		registry.put({{GenModelPrefix}}Package.eNS_URI, {{GenModelPrefix}}Package.eINSTANCE);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.emf.osgi.EPackageConfigurator#unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry)
	 */
	@Override
	public void unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {
		registry.remove({{GenModelPrefix}}Package.eNS_URI);
	}

}
