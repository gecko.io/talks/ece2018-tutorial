/**
 */
package {{basePackageName}}.{{EcorePackageName}};

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Example</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see {{basePackageName}}.{{EcorePackageName}}.{{GenModelPrefix}}Package#getExample()
 * @model
 * @generated
 */
public interface Example extends EObject {
} // Example
